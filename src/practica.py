# Función que suma dos enteros
def funcion1(num1, num2):
    suma = num1 + num2
    return suma
    pass


# Función que multiplica dos números decimales
def funcion2(num1, num2):
    multi = num1 * num2
    return multi
    pass


# Función que concatena dos cadenas de texto
def funcion3(str1, str2):
    concat = str1 + str2
    pass


# Función que verifica si un número entero es par
def funcion4(numero):
    if (numero % 2 == 0):
        return true 
    else: 
        return false
    pass


# Función que calcula el área de un triángulo dado su base y altura
def funcion5(base, altura):
    area = (base*altura)/2
    return area
    pass
